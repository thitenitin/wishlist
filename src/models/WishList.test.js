import { WishList, WishListItem } from "./WishList";
// import {jest} from '@jest/globals';

// jest.useFakeTimers();
test("can create a instance of a model", () => {
  const item = WishListItem.create({
    name: "Car seat covers",
    price: 31,
    // image:
    //   "https://www.autofurnish.com/autofurnish-3d-custom-pu-leather-car-seat-covers-for-toyota-innova-7s-ht-503-dawn",
  });

  expect(item.price).toBe(31);
  expect(item.image).toBe("");

  item.changeName("Wheel cover");
  expect(item.name).toBe('Wheel cover');
});

test("can create a wishlist", () => {
    const list = WishList.create({
        items:[ 
          {
            name: "Car seat covers",
            price: 12.40
        } 
      ]
    });

    expect(list.items.length).toBe(1);
    expect(list.items[0].price).toBe(12.40);
});

test("can add new items", () => {
  const list = WishList.create();
  list.addItem(WishListItem.create({
    name: "Wheels",
    price: 39
  }))

  expect(list.items.length).toBe(1);
    expect(list.items[0].name).toBe("Wheels");
})
